# PYTHIA configuration file.
# Generated on Wed Jun 8 11:35:37 PDT 2016 with the user supplied options:
# --with-hepmc2=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/HepMC/2.06.08/x86_64-slc6-gcc47-opt/
# --with-lhapdf6
# --with-lhapdf6=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc47-opt/
# --with-fastjet3-include=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/fastjet-install/include
# --with-fastjet3-lib=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/fastjet-install/lib/
# --with-boost=/usr/common/usg/software/boost/1.54.0
# --with-gzip-lib=/lib/
# --with-gzip-include=/usr/include/
# --with-gzip-bin=/bin

# Install directory prefixes.
PREFIX_BIN=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/pythia8/bin
PREFIX_INCLUDE=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/pythia8/include
PREFIX_LIB=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/pythia8/lib
PREFIX_SHARE=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/pythia8/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=false
CXX=g++
CXX_COMMON=-O2 -ansi -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname
LIB_SUFFIX=.so

# EVTGEN configuration.
EVTGEN_USE=false
EVTGEN_BIN=
EVTGEN_INCLUDE=
EVTGEN_LIB=

# FASTJET3 configuration.
FASTJET3_USE=true
FASTJET3_BIN=
FASTJET3_INCLUDE=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/fastjet-install/include
FASTJET3_LIB=/project/projectdirs/atlas/shapiro/AtlasProduction-19.2.4.14/Test/madgraph/fastjet-install/lib/

# HEPMC2 configuration.
HEPMC2_USE=true
HEPMC2_BIN=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/HepMC/2.06.08/x86_64-slc6-gcc47-opt//bin/
HEPMC2_INCLUDE=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/HepMC/2.06.08/x86_64-slc6-gcc47-opt//include
HEPMC2_LIB=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/HepMC/2.06.08/x86_64-slc6-gcc47-opt//lib

# HEPMC3 configuration.
HEPMC3_USE=false
HEPMC3_BIN=
HEPMC3_INCLUDE=
HEPMC3_LIB=

# LHAPDF5 configuration.
LHAPDF5_USE=false
LHAPDF5_BIN=
LHAPDF5_INCLUDE=
LHAPDF5_LIB=
LHAPDF5_PLUGIN=LHAPDF5.h

# LHAPDF6 configuration.
LHAPDF6_USE=true
LHAPDF6_BIN=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc47-opt//bin/
LHAPDF6_INCLUDE=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc47-opt//include
LHAPDF6_LIB=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc47-opt//lib
LHAPDF6_PLUGIN=LHAPDF5.h

# POWHEG configuration.
POWHEG_USE=false
POWHEG_BIN=
POWHEG_INCLUDE=
POWHEG_LIB=

# PROMC configuration.
PROMC_USE=false
PROMC_BIN=
PROMC_INCLUDE=
PROMC_LIB=

# ROOT configuration.
ROOT_USE=false
ROOT_BIN=
ROOT_INCLUDE=
ROOT_LIB=

# GZIP configuration.
GZIP_USE=true
GZIP_BIN=/bin
GZIP_INCLUDE=/usr/include/
GZIP_LIB=/lib/

# BOOST configuration.
BOOST_USE=true
BOOST_BIN=/usr/common/usg/software/boost/1.54.0/bin/
BOOST_INCLUDE=/usr/common/usg/software/boost/1.54.0/include
BOOST_LIB=/usr/common/usg/software/boost/1.54.0/lib
