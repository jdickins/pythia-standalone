Jennet's code for running showering in standalone Pythia
using main89, taking existing LHE files as input

In the generation directory:
cd generation/pythia8219/
Edit submit.sh to point to the directory where your 
input LHE files are stored. 
To run the showering on pdsf:
. submit.sh
The output will appear in output/

To edit the Pythia scripts:
cd pythia8.219/examples
Make desired changes
cd pythia8.219/
make clean; make
cd examples/
make main89
Remake tarball 
rm pythia8.219.tar.gz
tar -zcvf pythia8.219.tar.gz pythia8.219/

To run the Pythia's main89 interactively:
cd pythia2.819/examples
./main89 <script_name>.cmnd output.dat